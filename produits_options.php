<?php
/**
 * Options du plugin produitsau chargement
 *
 * @plugin     produits
 * @copyright  2014
 * @author     Les Développements Durables, http://www.ldd.fr
 * @licence    GNU/GPL
 * @package    SPIP\Produits\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/produits.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_produit' => 'Ajouter ce produit',

	// I
	'icone_creer_produit' => 'Créer un produit',
	'icone_modifier_produit' => 'Modifier ce produit',
	'info_1_produit' => 'Un produit',
	'info_aucun_produit' => 'Aucun produit',
	'info_nb_produits' => '@nb@ produits',
	'info_produits_auteur' => 'Les produits de cet auteur',

	// L
	'label_date_com' => 'Date',
	'label_descriptif' => 'Court descriptif',
	'label_prix_ht' => 'Prix Hors taxe',
	'label_reference' => 'Référence',
	'label_taxe' => 'Taxe',
	'label_texte' => 'Texte',
	'label_titre' => 'Titre',

	// P
	'produit_champ_taxe_explication' => 'Valeur par défaut sur le site : @taxe@. Ce champ peut être laissé vide.',

	// R
	'retirer_lien_produit' => 'Retirer ce produit',
	'retirer_tous_liens_produits' => 'Retirer tous les produits',

	// T
	'texte_ajouter_produit' => 'Ajouter un produit',
	'texte_changer_statut_produit' => 'Ce produit est :',
	'texte_creer_associer_produit' => 'Créer et associer un produit',
	'titre_langue_produit' => 'Langue de ce produit',
	'titre_logo_produit' => 'Logo de ce produit',
	'titre_produit' => 'Produit',
	'titre_produits' => 'Produits',
	'titre_produits_rubrique' => 'Produits de la rubrique'
);
